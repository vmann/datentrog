% rebase("template/toplevel.tpl", title=event)
% if get("success", False):
<div class="alert alert-success">Erfolgreich registriert.</div>
<form method="get">
  <button type="submit" class="btn btn-primary">Weitere Person registrieren</button>
</form>
% else:
<div class="alert alert-warning">Da sich eine <a href="https://www.br.de/nachrichten/bayern/gesundheitsaemter-stellen-breite-corona-kontaktnachverfolgung-ein">Benachrichtigung durch das Gesundheitsamt verzögern/ausfallen</a> kann, wird empfohlen die <a href="https://www.coronawarn.app">Corona-Warn-App</a> zu verwenden.</div>
<form method="post" autocomplete="on">
  <div class="form-group">
    <label for="event">Veranstaltungsort:</label>
    <input id="event" class="form-control" name="event" value="{{ get("event", "Heizhaus") }}" readonly>
  </div>
  <div class="form-group {{ ("has-success" if get("name", False) else "has-error") + " has-feedback" if get("feedback", False) else "" }}">
    <label for="name">Name, Vorname:</label>
    <input id="name" class="form-control" placeholder="Söder, Markus" name="name" value="{{ get("name", "") }}">
    % if get("feedback", False):
    <span class="glyphicon {{ "glyphicon-ok" if get("name", False) else "glyphicon-remove" }} form-control-feedback"></span>
    % end
  </div>
  <div class="form-group {{ ("has-success" if get("address", False) else "has-error") + " has-feedback" if get("feedback", False) else "" }}">
    <label for="address">Adresse:</label>
    <input id="address" class="form-control" placeholder="Jakobstraße 46, 90402 Nürnberg" name="address" value="{{ get("address", "") }}">
    % if get("feedback", False):
    <span class="glyphicon {{ "glyphicon-ok" if get("address", False) else "glyphicon-remove" }} form-control-feedback"></span>
    % end
  </div>
  <div class="form-group {{ ("has-success" if get("contact", False) else "has-error") + " has-feedback" if get("feedback", False) else "" }}">
    <label for="contact">Email/Telefon:</label>
    <input id="contact" class="form-control" placeholder="markus.soeder@soeder.de" name="contact" value="{{ get("contact", "") }}">
    % if get("feedback", False):
    <span class="glyphicon {{ "glyphicon-ok" if get("contact", False) else "glyphicon-remove" }} form-control-feedback"></span>
    % end
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success">Registrieren</button>
  </div>
</form>
<div class="alert alert-info">Die Daten werden nach 1 Monat gelöscht und ausschließlich zur Corona-Kontaktverfolgung verwendet.</div>
% end
