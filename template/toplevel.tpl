<!DOCTYPE html>
<html>
  <head>
    <title>Corona-Kontaktformular - {{ get("title", "Heizhaus") }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap-3.4.1-dist/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      <h1>Corona-Kontaktformular</h1>
      {{!base}}
      <div class="pull-right hidden-print">
        <a href="https://git.0x90.space/vmann/datentrog"><span class="small text-muted">Code</span></a>
        <a href="https://0x90.space/impressum/#impressum"><span class="small text-muted">Impressum</span></a>
      </div>
    </div>
  </body>
</html>
