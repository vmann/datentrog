% rebase("template/toplevel.tpl", title="Neu erstellen")
<form method="post">
  <div class="form-group {{ ("has-success" if get("event", False) else "has-error") + " has-feedback" if get("feedback", False) else "" }}">
    <label for="event">Veranstaltungsort:</label>
    <input id="event" class="form-control" placeholder="Heizhaus" name="event" value="{{ get("event", "") }}">
    % if get("feedback", False):
    <span class="glyphicon {{ "glyphicon-ok" if get("event", False) else "glyphicon-remove" }} form-control-feedback"></span>
    % end
  </div>
  <div class="form-group">
    <button type="submit" class="btn btn-success">Formular erstellen</button>
  </div>
</form>
