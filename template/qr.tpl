% rebase("template/toplevel.tpl", title=event)
<div class="hidden-print">
  <a class="btn btn-default" href="{{ url }}">Zum Formular</a>
  <button class="btn btn-primary" onclick="window.print()">Drucken</button>
</div>
<img class="img-responsive" src="img/{{ event }}.png" alt="QR-Code">
<p class="lead">{{ url }}</p>
